package com.example.onlyme.myapplication;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by only me on 12/05/2017.
 */

public interface lalulintasinterface {

    String url = "http://lalulintasmedan.96.lt/api/v1";

    //insert data
    @FormUrlEncoded
    @POST("/userposisis")
    void createdata(
            @Field("waktu") String waktu,
            @Field("long") String longitude,
            @Field("lat") String latitude,
            @Field("apt") String altitude,
            @Field("kecepatan") String kecepatan,
            Callback<Posisi> response);

   // void createdata(String currentDateTimeString, String latitude, String longitude, String altitude, String speed, Callback<data> done);
}

