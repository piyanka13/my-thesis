package com.example.onlyme.myapplication;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.text.DateFormat;
import java.util.Date;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by only me on 10/05/2017.
 */

public class MyServices extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    private static final String TAG = "MapsActivity";
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private LocationManager mLocationManager;

    private SQLiteDatabase db;
    private LocationRequest mLocationRequest;
    private com.google.android.gms.location.LocationListener listener;
    private long UPDATE_INTERVAL = 2* 1000;  /* 10 secs */
    private long FASTEST_INTERVAL = 2000; /* 2 sec */

    private LocationManager locationManager;
    private TelephonyManager telephonyManager;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */

   public int onStartCommand(Intent intent, int flags, int startId){

       mGoogleApiClient = new GoogleApiClient.Builder(this)
               .addConnectionCallbacks(this)
               .addOnConnectionFailedListener(this)
               .addApi(LocationServices.API)
               .build();
       mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
       telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            showToast();
        return START_STICKY;
    }

    private void showToast() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.e("testservice", "Halo");
                // Toast.makeText(getBaseContext(),"hellosdsj",Toast.LENGTH_SHORT).show();
                onStart();
                showToast();

            }
        }, 1000);
    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onConnected(Bundle bundle) {
        Log.e("test", "onConnected start");
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Log.e("test", "onConnected lewat");
        startLocationUpdates();

        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        Log.e("test", "mLocation Called");
        if (mLocation == null) {
            startLocationUpdates();
        }
        if (mLocation != null) {

            // mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
        } else {
            Toast.makeText(this, "Location not Detected", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection Suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed. Error: " + connectionResult.getErrorCode());}

    protected void onStart() {
   //     super.onStart();
        Log.e("test", "onStart");
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
            Log.e("test", "google api connect");
        }
    }

    protected void onStop() {
        //super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    protected void startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                mLocationRequest, this);
        Log.d("reque", "--->>>>");
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onLocationChanged(Location location) {

        db = openOrCreateDatabase("Track.db",Context.MODE_PRIVATE,null);
        db.execSQL("CREATE TABLE IF NOT EXISTS tracks(waktu varchar, longitude varchar, latitude varchar, altitude varchar,kecepatan varchar);");

        final String  waktu = DateFormat.getDateTimeInstance()
                .format(new Date());
        final String latitude  = String.valueOf(location.getLatitude());
        final String longitude = String.valueOf(location.getLongitude());
        final String kecepatan  = String.valueOf(location.getSpeed());
        final String altitude  = telephonyManager.getDeviceId();

        db.execSQL("INSERT INTO tracks VALUES ('"+waktu+"','"+longitude+"','"+latitude+"','"+altitude+"','"+kecepatan+"');");
        final String msg = "Updated Location:\n"
                +"Lat: "+latitude+""
                +"Long: "+longitude+"\n"
                +"Speed: "+kecepatan+"\n"
                +"Date : "+waktu+"\n";

        Log.e("Lat: ", "" + location.getLatitude());
        Log.e("Long :", "" + location.getLongitude());
        Log.e("Alti :", "" + location.getAltitude());
        Log.e("Speed:", "" + location.getSpeed()*3600/1000);
        Log.e("time: ","" + telephonyManager.getDeviceId());


        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(lalulintasinterface.url)
                //.setClient(new OkClient(SelfSigningClient.createClient()))
                //.setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        final lalulintasinterface lalulintasinterface = restAdapter.create(com.example.onlyme.myapplication.lalulintasinterface.class);
        lalulintasinterface.createdata(
                waktu,
                longitude,
                latitude,
                altitude,
                kecepatan,
                new Callback<Posisi>(){
                    @Override
                    public void success(Posisi data, Response response) {
                        //  Toast.makeText(getBaseContext(),"Done", Toast.LENGTH_LONG).show();
                        //Toast.makeText(getBaseContext(),msg+"\n Done..", Toast.LENGTH_SHORT).show();
                        Log.e("",""+response.getReason());

                    }
                    @Override
                    public void failure(RetrofitError error) {
                        Log.e("error: ",""+error.getMessage());
                        Log.e("response",""+error.getResponse());


                    }
                }
        );
    }

    }


