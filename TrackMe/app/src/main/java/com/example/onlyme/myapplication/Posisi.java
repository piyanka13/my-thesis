package com.example.onlyme.myapplication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Posisi {

 @SerializedName("waktu")
 @Expose
 private String waktu;
 @SerializedName("long")
 @Expose
 private String _long;
 @SerializedName("lat")
 @Expose
 private String lat;
 @SerializedName("apt")
 @Expose
 private String apt;
 @SerializedName("kecepatan")
 @Expose
 private String kecepatan;
 @SerializedName("id")
 @Expose
 private Integer id;

 public String getWaktu() {
  return waktu;
 }

 public void setWaktu(String waktu) {
  this.waktu = waktu;
 }

 public String getLong() {
  return _long;
 }

 public void setLong(String _long) {
  this._long = _long;
 }

 public String getLat() {
  return lat;
 }

 public void setLat(String lat) {
  this.lat = lat;
 }

 public String getApt() {
  return apt;
 }

 public void setApt(String apt) {
  this.apt = apt;
 }

 public String getKecepatan() {
  return kecepatan;
 }

 public void setKecepatan(String kecepatan) {
  this.kecepatan = kecepatan;
 }

 public Integer getId() {
  return id;
 }

 public void setId(Integer id) {
  this.id = id;
 }

}